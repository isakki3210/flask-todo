# Flask Todo Application

This is a simple Flask application for managing tasks in a to-do list. Users can add, complete, undo, and delete tasks.

## File Structure

The Flask Todo project follows a standard Flask application structure:

```plaintext
Flask-Todo-App/
|-- app.py
|-- templates/
|   |-- index.html
|-- venv/
|-- .gitignore
|-- .env
|-- requirements.txt
|-- README.md
```

## Installation

Follow these steps to set up the Flask Todo application on your local development environment:

1. **Clone the Repository**:

    ```bash
    git clone https://github.com/YourUsername/Flask-Todo-App.git
    cd Flask-Todo-App
    ```

2. **Create and Activate Virtual Environment**:

    ```bash
    python -m venv venv
    source venv/bin/activate   # On Windows, use: .\venv\Scripts\activate
    ```

3. **Install Dependencies**:

    ```bash
    pip install -r requirements.txt
    ```

4. **Set Environment Variables:**

    Create a file named `.env` in the project's root directory and add:

    ```plaintext
    MONGO_URI="your_mongo_uri"
    ```

    Replace "your_mongo_uri" with your MongoDB connection string.

5. **Run the Application**:

    ```bash
    python app.py
    ```

6. **Access the Application**:

    Open your web browser and navigate to [http://localhost:5000](http://localhost:5000) to access the Todo application.

Now, your MongoDB URI is loaded from the `.env` file, providing a more secure configuration.