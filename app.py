# app.py

from flask import Flask, render_template, request, redirect, url_for, flash
from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired
from pymongo import MongoClient
from bson import ObjectId
from dotenv import load_dotenv
import secrets
import os

load_dotenv()
app = Flask(__name__)
app.config['SECRET_KEY'] = secrets.token_hex(16)   # Set a strong secret key

# Use environment variables for MongoDB connection
mongo_uri = os.getenv("MONGO_URI")
# print(f"MongoDB URI: {mongo_uri}")

# Make a successful MongoClient and db
client = MongoClient(mongo_uri)
db = client["Isakki_test"]
todos = db["todos"]

# Load an Index page
@app.route("/")
def index():
    try:
        tasks = todos.find()
        task_list = list(tasks)
        return render_template("index.html", tasks=task_list)
    except Exception as e:
        print(f"Error loading tasks: {e}")
        return render_template("index.html", tasks=[])


# Define a TaskForm for form handling
class TaskForm(FlaskForm):
    task = StringField('Task', validators=[DataRequired()])
    submit = SubmitField('Add')

# Add a task to the MongoDB
@app.route("/add", methods=["POST"])
def add():
    form = TaskForm(request.form)
    task = request.form.get("task")

    if task:
        try:
            todos.insert_one({"task": task, "done": False})
            flash(f"Task '{task}' added successfully.", "success")
        except Exception as e:
            flash("Error: Unable to add the task. Please try again.", "danger")
    else:
        flash("Error: Please enter a valid task.", "danger")

    return redirect(url_for("index"))

# Complete a task
@app.route("/complete/<string:task_id>")
def complete(task_id):
    try:
        task_id = ObjectId(task_id)
        task = todos.find_one({"_id": task_id})
        flash(f"Task '{task['task']}' completed successfully.", "success")
        todos.update_one({"_id": task_id}, {"$set": {"done": True}})
    except Exception as e:
        flash(f"Error completing task: {e}", "danger")

    return redirect(url_for("index"))

# Undo the task
@app.route("/undo/<string:task_id>")
def undo(task_id):
    try:
        task_id = ObjectId(task_id)
        task = todos.find_one({"_id": task_id})
        flash(f"Task '{task['task']}' undone successfully.", "info")
        todos.update_one({"_id": task_id}, {"$set": {"done": False}})
    except Exception as e:
        flash(f"Error undoing the task: {e}", "danger")

    return redirect(url_for("index"))

# Delete a task
@app.route("/delete/<string:task_id>")
def delete(task_id):
    try:
        task_id = ObjectId(task_id)
        task = todos.find_one({"_id": task_id})
        flash(f"Task '{task['task']}' deleted successfully.", "danger")
        todos.delete_one({"_id": task_id})
    except Exception as e:
        flash(f"Error deleting task: {e}", "danger")

    return redirect(url_for("index"))
